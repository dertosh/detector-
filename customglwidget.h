#ifndef OPENGLWIDGET_H
#define OPENGLWIDGET_H

#include <QDebug>
#include <QtOpenGL/QGLWidget>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
using namespace cv;

class QTimer;
struct CvCapture;
struct _IplImage;

class CustomGLWidget : public QGLWidget {
  Q_OBJECT;

 private:
  QTimer *m_timer;
  VideoCapture m_capture;
  Mat m_image;
  int fontFace = FONT_HERSHEY_SCRIPT_SIMPLEX;
  double fontScale = 2;
  int thickness = 3;
  int baseline = 0;

 public:
  CustomGLWidget(QWidget *parent = NULL);
  ~CustomGLWidget();

 protected:
  void initializeGL();
  void paintGL();

 private slots:
  void captureFrame();
};

#endif  // CUSTOMGLWIDGET_H
