/*************************************************************************
   Copyright (c) 2010 Rafael Palomar
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/

#include "customglwidget.h"
#include <QMessageBox>
#include <QTimer>
#include <iostream>

CustomGLWidget::CustomGLWidget(QWidget *parent) : QGLWidget(parent) {
  // Initialize variable members
  // m_image = cvLoadImage("f://1.png", CV_LOAD_IMAGE_COLOR);

  // Initialize variable members
  m_timer = new QTimer();
  m_capture.open(0);
  if (!m_capture.isOpened()) {  // check if we succeeded
    QMessageBox::critical(this, "Error",
                          "Error initializing capture from WebCam");
  }

  // Get an initial frame from the webcam
  m_capture >> m_image;
  // Connect the timer signal with the capture action
  connect(m_timer, SIGNAL(timeout()), this, SLOT(captureFrame()));

  // Start the timer scheduled for firing every 33ms (30fps)
  m_timer->start(33);
}
CustomGLWidget::~CustomGLWidget() { m_capture.release(); }
void CustomGLWidget::initializeGL() {
  glViewport(0, 0, this->width(), this->height());  // Adjust the viewport
  glMatrixMode(GL_PROJECTION);  // Adjust the projection matrix
  glLoadIdentity();
  glOrtho(-this->width() / 2, this->width() / 2, this->height() / 2,
          -this->height() / 2, -1, 1);
}
void CustomGLWidget::paintGL() {
  // Clear the color buffer
  glClear(GL_COLOR_BUFFER_BIT);
  // Set the raster position
  /*The position seems to be the inverse because the rendering is affected by
   * the glPixelZoom call.*/
  glRasterPos2i(-this->width() / 2, -this->height() / 2);
  // Inver the image (the data coming from OpenCV is inverted)
  glPixelZoom(1.0f, -1.0f);
  // Draw image from OpenCV capture
  glDrawPixels(m_image.cols, m_image.rows, GL_BGR, GL_UNSIGNED_BYTE,
               m_image.data);
}
void CustomGLWidget::captureFrame() {
  QPoint mousePoint = this->mapToGlobal(QCursor::pos());

  // center the text

  m_capture >> m_image;

  glDraw();  // Draw the scene
}
