#-------------------------------------------------
#
# Project created by QtCreator 2019-04-20T17:22:40
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = detector-
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
	main.cpp \
	mainwindow.cpp \
    customglwidget.cpp

HEADERS += \
	mainwindow.h \
    customglwidget.h

FORMS += \
	mainwindow.ui

LIBS += -lglu32 -lopengl32

OPENCV_SDK_DIR = G:\opencv-build\install
OPENCV_SDK_V = 343

# more correct variant, how set includepath and libs for mingw
# add system variable: OPENCV_SDK_DIR=D:/opencv/opencv-build/install
# read http://doc.qt.io/qt-5/qmake-variable-reference.html#libs

INCLUDEPATH += $${OPENCV_SDK_DIR}/include

LIBS += -L$${OPENCV_SDK_DIR}/x86/mingw/lib \
	-lopencv_core$${OPENCV_SDK_V}        \
	-lopencv_highgui$${OPENCV_SDK_V}     \
	-lopencv_imgcodecs$${OPENCV_SDK_V}   \
	-lopencv_imgproc$${OPENCV_SDK_V}     \
	-lopencv_features2d$${OPENCV_SDK_V}  \
	-lopencv_calib3d$${OPENCV_SDK_V} \
	-lopencv_videoio$${OPENCV_SDK_V}

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
