#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QLabel>
#include <QString>
#include <QVBoxLayout>
#include <QtOpenGL/QGLWidget>
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "CustomGLWidget.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  glWidget = new CustomGLWidget();
  ui->verticalLayout->addWidget(glWidget);

  // read an image
  // cv::Mat image = cv::imread("f://1.jpg", 1);
  // create image window named "My Image"
  // cv::namedWindow("My Image");
  // show the image on window
  // cv::imshow("My Image", image);
}

MainWindow::~MainWindow() { delete ui; }
